﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
    Vector3 offSet = new Vector3 (0f, 2f, 0f);
    // Start is called before the first frame update
    void Start()
    {
        Destroy(this.gameObject, 0.25f);
        transform.localPosition += offSet;
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
