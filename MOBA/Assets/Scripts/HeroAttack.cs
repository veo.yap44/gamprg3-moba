﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroAttack : MonoBehaviour
{
    public enum HeroType { Melee, Ranged };
    public HeroType heroType;

    public GameObject targetedEnemy;
    public float attackRange;
    public float rotateSpeedForAttack;

    private PlayerMovement moveScript;

    public bool basicAtkIdle = false;
    public bool isHeroAlive;
    public bool performMeleeAttack = true;
    public float attackTime;
    private float shootNext = 0f;
    private HeroStats stats;
    // Start is called before the first frame update
    void Start()
    {
        stats = GetComponent<HeroStats>();

        moveScript = GetComponent<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (targetedEnemy != null)
        {
            if (Vector3.Distance(gameObject.transform.position, targetedEnemy.transform.position) > attackRange)
            {
                moveScript.hero.SetDestination(targetedEnemy.transform.position);
                moveScript.hero.stoppingDistance = attackRange;
                

            }
            else
            {
                Debug.Log("Here");
                Quaternion lookAt = Quaternion.LookRotation(targetedEnemy.transform.position - transform.position);
                float rotationY = Mathf.SmoothDampAngle(transform.eulerAngles.y, lookAt.eulerAngles.y, ref moveScript.rotVelocity, rotateSpeedForAttack * (Time.deltaTime * 5));
                transform.eulerAngles = new Vector3(0, rotationY, 0);
                attackTime += Time.deltaTime;
                moveScript.hero.SetDestination(transform.position);
                //Melee Attack
                if (heroType == HeroType.Melee)
                {
                    moveScript.hero.stoppingDistance = 0;
                    Debug.Log("Attack The Minion");


                    if (attackTime > ((1.7f / (1f + (stats.attackSpeed / 100)))))
                    {
                        targetedEnemy.GetComponent<HealthComponent>().TakeDamage(this.GetComponent<Damage>().calculateFinalDamage());
                        Debug.Log(this.GetComponent<Damage>().calculateFinalDamage());
                        if (targetedEnemy.GetComponent<HealthComponent>().health <= 0)
                        {
                            if (targetedEnemy.gameObject.name == "Tower")
                            {
                                this.GetComponent<HeroStats>().gainExp(100);
                                this.GetComponent<HeroStats>().gainGold(200);
                                targetedEnemy = null;

                            }
                            else if (targetedEnemy.gameObject.name == "Hero")
                            {

                                if (targetedEnemy.GetComponent<HeroStats>().level >= 25)
                                {
                                    this.GetComponent<HeroStats>().gainExp(targetedEnemy.GetComponent<HeroStats>().expBounty[24]);
                                    targetedEnemy = null;

                                }
                                else
                                {
                                    this.GetComponent<HeroStats>().gainExp(targetedEnemy.GetComponent<HeroStats>().expBounty[targetedEnemy.GetComponent<HeroStats>().level - 1]);
                                    targetedEnemy = null;

                                }
                                this.GetComponent<HeroStats>().gainGold(110+(targetedEnemy.GetComponent<HeroStats>().level*8));
                            }
                            else if (targetedEnemy.gameObject.name == "Barracks")
                            {
                                this.GetComponent<HeroStats>().gainExp(100);
                                this.GetComponent<HeroStats>().gainGold(125);
                                targetedEnemy = null;
                            }
                            else if (targetedEnemy.gameObject.name == "Ancient")
                            {
                                this.GetComponent<HeroStats>().gainExp(100);
                                targetedEnemy = null;

                            }
                            else
                            {
                                Debug.Log("Levl");
                                this.GetComponent<HeroStats>().gainExp(targetedEnemy.GetComponent<Creeps>().exp);
                                this.GetComponent<HeroStats>().gainGold((int)targetedEnemy.GetComponent<Creeps>().goldBounty);
                                Destroy(targetedEnemy);
                                targetedEnemy = null;

                            }
                            targetedEnemy = null;



                            attackTime = 0;
                        }

                    }

                }
                //Ranged Attack
                if (heroType == HeroType.Ranged)
                {

                    Debug.Log("Player Attack1");
                    if (attackTime > ((1.7f / (1f + (stats.attackSpeed / 100)))))
                    {
                        Debug.Log("Player Attack");
                        this.gameObject.GetComponent<RangedAttack>().Shoot(targetedEnemy, this.GetComponent<Damage>().calculateFinalDamage(), 15);
                        if (targetedEnemy.GetComponent<HealthComponent>().health <= 0)
                        {
                            if (targetedEnemy.gameObject.name == "Tower")
                            {
                                this.GetComponent<HeroStats>().gainExp(100);
                                this.GetComponent<HeroStats>().gainGold(200);
                                targetedEnemy = null;

                            }
                            else if (targetedEnemy.gameObject.name == "Hero")
                            {

                                if (targetedEnemy.GetComponent<HeroStats>().level >= 25)
                                {
                                    this.GetComponent<HeroStats>().gainExp(targetedEnemy.GetComponent<HeroStats>().expBounty[24]);
                                    targetedEnemy = null;

                                }
                                else
                                {
                                    this.GetComponent<HeroStats>().gainExp(targetedEnemy.GetComponent<HeroStats>().expBounty[targetedEnemy.GetComponent<HeroStats>().level - 1]);
                                    targetedEnemy = null;

                                }
                                this.GetComponent<HeroStats>().gainGold(110 + (targetedEnemy.GetComponent<HeroStats>().level * 8));
                            }
                            else if (targetedEnemy.gameObject.name == "Barracks")
                            {
                                this.GetComponent<HeroStats>().gainExp(100);
                                this.GetComponent<HeroStats>().gainGold(125);
                                targetedEnemy = null;
                            }
                            else if (targetedEnemy.gameObject.name == "Ancient")
                            {
                                this.GetComponent<HeroStats>().gainExp(100);
                                targetedEnemy = null;

                            }
                            else
                            {
                                Debug.Log("Levl");
                                this.GetComponent<HeroStats>().gainExp(targetedEnemy.GetComponent<Creeps>().exp);
                                this.GetComponent<HeroStats>().gainGold((int)targetedEnemy.GetComponent<Creeps>().goldBounty);
                                Destroy(targetedEnemy);
                                targetedEnemy = null;

                            }

                            targetedEnemy = null;
                        }

                        attackTime = 0;
                    }
                }

                //Stops Attack
                if (moveScript.stop == true)
                {
                    targetedEnemy = null;
                }
            }

        }

    }
}
