﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class HealthComponent : MonoBehaviour
{
    public GameObject damageText;
    public float health;
    public bool isDestroyed;
    public List<GameObject> towerObjects;
    private float timer;
    public TowerManager[] towerManagers;
    // Start is called before the first frame update
    void Start()
    {
        isDestroyed = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //Object Takes Damage
    public void TakeDamage(float dmg)
    {
        if (this.gameObject.name == "Tower")
        {
            if(this.gameObject.GetComponent<Tower>().isInvunerable == true)
            {
                health -= 0;
                
            }
            else
            {
                health -= dmg;
                
                ShowDamage(dmg);
                if (health <= 0)
                {
                    Die();
                }
            }
        }
        else if (this.gameObject.name == "Barracks")
        {
            Debug.Log("Here2");
            if (this.gameObject.GetComponent<Barracks>().isInvunerable == true)
            {
                Debug.Log("Here3");
                health -= 0;

            }
            else
            {
                Debug.Log("Here1");
                health -= dmg;
                ShowDamage(dmg);
                if (health <= 0)
                {
                    Die();
                }
            }
        }
        else if (this.gameObject.name == "Ancient")
        {
            Debug.Log("Here2");
            if (this.gameObject.GetComponent<Ancient>().isInvunerable == true)
            {
                Debug.Log("Here3");
                health -= 0;

            }
            else
            {
                Debug.Log("Here1");
                health -= dmg;
                ShowDamage(dmg);
                if (health <= 0)
                {
                    Die();
                }
            }
        }

        else
        {
            health -= dmg;
            ShowDamage(dmg);
            if (health <= 0)
            {
                Die();
            }
        }
        
    }
    //Object Dies
    public void Die()
    {
        if(this.gameObject.name == "Barracks")
        {
            this.gameObject.GetComponent<MeshRenderer>().enabled = false;
            this.gameObject.GetComponent<BoxCollider>().enabled = false;
        }
        else if(this.gameObject.name == "Tower")
        {
           foreach(GameObject t in towerObjects)
            {
                t.GetComponent<MeshRenderer>().enabled = false;
            }
            this.gameObject.GetComponent<BoxCollider>().enabled = false;
            this.gameObject.GetComponent<Tower>().enabled = false;
            isDestroyed = true;
            
           
        }
        else if(this.gameObject.name == "Hero")
        {
            health = 0;
            foreach(GameObject e in towerObjects)
            {
                e.GetComponent<SkinnedMeshRenderer>().enabled = false;
            }

            this.gameObject.GetComponent<SphereCollider>().enabled = false;
            this.gameObject.GetComponent<CapsuleCollider>().enabled = false;
            this.gameObject.GetComponent<PlayerMovement>().hero.speed = 0f;
            this.gameObject.GetComponent<PlayerMovement>().respawnText.enabled = true;
            this.gameObject.GetComponent<PlayerMovement>().respawnTimer.enabled= true;
            if (this.gameObject.tag == "Radiant")
            {
                this.gameObject.GetComponent<PlayerMovement>().hero.Warp(this.gameObject.GetComponent<PlayerMovement>().spawnPoint[0].transform.position);
                this.gameObject.GetComponent<PlayerMovement>().isDead = true;
            }
            else if (this.gameObject.tag == "Dire")
            {
                this.gameObject.GetComponent<PlayerMovement>().hero.Warp(this.gameObject.GetComponent<PlayerMovement>().spawnPoint[1].transform.position);
                this.gameObject.GetComponent<PlayerMovement>().isDead = true;
            }
            

        }
        else if (this.gameObject.name == "Ancient")
        {
            health = 0;
            foreach (GameObject e in towerObjects)
            {
                e.GetComponent<MeshRenderer>().enabled = false;
            }
        }
        else
        {
            
            timer += Time.deltaTime;
            if (timer>0.3f)
            {
                Destroy(this.gameObject);
                timer = 0;
            }
            
        }
    }
    //Display Damage
    public void ShowDamage(float dmg)
    {
        var dmgText = Instantiate(damageText, transform.position, Quaternion.identity, transform);
        dmgText.GetComponent<TextMeshPro>().text = dmg.ToString("F0");
    }
}
