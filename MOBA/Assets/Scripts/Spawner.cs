﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject meleeSpawnee;
    public GameObject rangedSpawnee;
    public GameObject siegeSpawnee;
    public GameObject meleeSuperSpawnee;
    public GameObject rangedSuperSpawnee;
    public GameObject siegeSuperSpawnee;
    public GameObject meleeBarracks;
    public GameObject rangedBarracks;

    
    private float timer;
    private float delayTimer;
    private float rangedEnemyCount;
    private float meleeEnemyCount;
    public int waveCount;
    public bool superCreepsEnabled;
    public string waypointPath;

    // Use this for initialization
    void Start()
    {
        superCreepsEnabled = false;
        waveCount = 1;
    }
    private void FixedUpdate()
    {
        //Checks if SuperCreeps are Enabled
        if (meleeBarracks.GetComponent<HealthComponent>().health <= 0 && rangedBarracks.GetComponent<HealthComponent>().health <= 0)
        {
            superCreepsEnabled = true;
        }
        timer += Time.deltaTime;
        delayTimer += Time.deltaTime;
        if (delayTimer > 1f)
        {
            SpawnObject();

            delayTimer = 0f;
        }
        if (timer > 30.0f)
        {
            //Spawner
            meleeEnemyCount = 0;
            rangedEnemyCount = 0;
            timer = 0f;
            waveCount++;
        }

    }

    public void SpawnObject()
    {
        //Spawner
        if (superCreepsEnabled == true)
        {
            if (waveCount < 10)
            {
                if (rangedEnemyCount < 1)
                {
                    GameObject enemy = Instantiate(rangedSuperSpawnee, transform.position, transform.rotation);
                    enemy.GetComponent<CreepMovement>().waypointTags = this.waypointPath;
                    rangedEnemyCount += 1;
                }
                if (meleeEnemyCount < 3)
                {

                    GameObject enemy = Instantiate(meleeSuperSpawnee, transform.position, transform.rotation);
                    enemy.GetComponent<CreepMovement>().waypointTags = this.waypointPath;
                    meleeEnemyCount += 1;
                }


            }
            if (waveCount == 10)
            {
                GameObject enemy = Instantiate(siegeSuperSpawnee, transform.position, transform.rotation);
                enemy.GetComponent<CreepMovement>().waypointTags = this.waypointPath;
                waveCount = 0;
            }
        }
        else
        {
            if (waveCount < 10)
            {
                if (rangedEnemyCount < 1)
                {
                    GameObject enemy = Instantiate(rangedSpawnee, transform.position, transform.rotation);
                    enemy.GetComponent<CreepMovement>().waypointTags = this.waypointPath;
                    rangedEnemyCount += 1;
                }
                if (meleeEnemyCount < 3)
                {

                    GameObject enemy = Instantiate(meleeSpawnee, transform.position, transform.rotation);
                    enemy.GetComponent<CreepMovement>().waypointTags = this.waypointPath;
                    meleeEnemyCount += 1;
                }


            }
            if (waveCount == 10)
            {
                GameObject enemy = Instantiate(siegeSpawnee, transform.position, transform.rotation);
                enemy.GetComponent<CreepMovement>().waypointTags = this.waypointPath;
                waveCount = 0;
            }
        }
        
        
        
        
    }

   
}
