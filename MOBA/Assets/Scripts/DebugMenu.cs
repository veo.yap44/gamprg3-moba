﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugMenu : MonoBehaviour
{
    public Canvas debugMenu;
    private bool debugIsUp;
    public HeroStats stats;
    public Spawner[] spawns;
    // Start is called before the first frame update
    void Start()
    {
        debugIsUp = false;
        debugMenu.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.F12))
        {
            if(debugIsUp == false)
            {
                debugMenu.enabled = true;
                debugIsUp = true;
            }
            else if(debugIsUp ==true)
            {
                debugMenu.enabled = false;
                debugIsUp = false;
            }
        }
    }

    public void LevelUpOne()
    {
        if(stats.level < 25)
        {
            stats.LevelUp();
        }

        
    }

    public void MaxLevel()
    {
        if (stats.level < 25)
        {
            

            for (int i = stats.level; i < 25; i++)
            {
                stats.LevelUp();
            }
        }
    }

    public void IncreaseGameSpeed()
    {
        
        
        
       Time.timeScale = 5;
        
        
    }
    public void DecreaseGameSpeed()
    {
        Time.timeScale /= 5;
    }

    public void EnableSuperCreeps()
    {
        foreach(Spawner s in spawns)
        {
            s.superCreepsEnabled = true;
        }
    }
    public void DisableSuperCreeps()
    {
        foreach (Spawner s in spawns)
        {
            s.superCreepsEnabled = false;
        }
    }
}
