﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Ancient : MonoBehaviour
{
    public Canvas EndScreen;
    public TextMeshProUGUI winnerText;
    public float armor;
    public float magicResist;
    public float hpRegen;
    float health;
    public bool isInvunerable;
    // Start is called before the first frame update
    void Start()
    {
        health = this.GetComponent<HealthComponent>().health;
        EndScreen.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        health = this.GetComponent<HealthComponent>().health;
        if (health<=0)
        {
            Time.timeScale = 0.3f;
            if(this.gameObject.tag == "Dire")
            {
                EndScreen.enabled = true;
                winnerText.text = "Radiant Victory";
            }
            else if(this.gameObject.tag =="Radiant")
            {
                EndScreen.enabled = true;
                winnerText.text = "Dire Victory";
            }
        }
    }
}
