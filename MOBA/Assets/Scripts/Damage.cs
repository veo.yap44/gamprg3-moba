﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour
{
    float finalDamage;
    float evasion;
    float armorReduction;
    float baseDamage;
    float totalResist;
    float totalReduction;
    float totalMagicReduce;
    private HeroStats stats;
    private HeroAttack target;
    private void Start()
    {
        target = GetComponent<HeroAttack>();
        stats = GetComponent<HeroStats>();
     
    }
    private void Update()
    {
        
    }
    public float calculateFinalDamage()
    {
        finalDamage = (getBaseDamage() * (CalculateMagicResist()) * calculateArmorReduction());
        return finalDamage;
    }
    public float calculateArmorReduction()
    {
        if (target.targetedEnemy.gameObject.name == "Tower")
        {
            armorReduction = 1f - ((0.052f * target.targetedEnemy.GetComponent<Tower>().armor) / (0.9f + 0.048f * Mathf.Abs(target.targetedEnemy.GetComponent<Tower>().armor)));
        }
        else if (target.targetedEnemy.gameObject.name == "Hero")
        {
            armorReduction = 1f - ((0.052f * target.targetedEnemy.GetComponent<HeroStats>().armor) / (0.9f + 0.048f * Mathf.Abs(target.targetedEnemy.GetComponent<HeroStats>().armor)));
        }
        else if (target.targetedEnemy.gameObject.name == "Barracks")
        {
            armorReduction = 1f - ((0.052f * target.targetedEnemy.GetComponent<Barracks>().armor) / (0.9f + 0.048f * Mathf.Abs(target.targetedEnemy.GetComponent<Barracks>().armor)));
        }
        else if (target.targetedEnemy.gameObject.name == "Ancient")
        {
            armorReduction = 1f - ((0.052f * target.targetedEnemy.GetComponent<Ancient>().armor) / (0.9f + 0.048f * Mathf.Abs(target.targetedEnemy.GetComponent<Ancient>().armor)));
        }
        else
        {
            armorReduction = 1f - ((0.052f * target.targetedEnemy.GetComponent<Creeps>().armor) / (0.9f + 0.048f * Mathf.Abs(target.targetedEnemy.GetComponent<Creeps>().armor)));
        }
        return armorReduction;
    }
    

    public float getBaseDamage()
    {
        baseDamage = stats.damage;
        return baseDamage;
    }
    public float CalculateMagicResist()
    {
        if (target.targetedEnemy.gameObject.name == "Tower")
        {
            totalResist= 1f - ((1 * target.targetedEnemy.GetComponent<Tower>().magicResist));
        }
        else if(target.targetedEnemy.gameObject.name == "Hero")
        {
            totalResist = 1f - ((1 * target.targetedEnemy.GetComponent<HeroStats>().magicResist));
        }
        else if (target.targetedEnemy.gameObject.name == "Barracks")
        {
            totalResist = 1f - ((1 * target.targetedEnemy.GetComponent<Barracks>().magicResist));
        }
        else if (target.targetedEnemy.gameObject.name == "Ancient")
        {
            totalResist = 1f - ((1 * target.targetedEnemy.GetComponent<Ancient>().magicResist));
        }
        else
        {
            totalResist = 1f - ((1 * target.targetedEnemy.GetComponent<Creeps>().magicResistance));
        }
        return totalResist;
    }
    
}
