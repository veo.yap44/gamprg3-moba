﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerProtection : MonoBehaviour
{
    public GameObject tower;
    public string faction;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == faction && other.name != "Barracks" && other.name != "Tower" && other.name != "Ancient"&& other.tag != "Other"&&other.tag!="Floor"&&other.name!="Crystal")
        {
            if (other.name == "Hero")
            {
                if (other.GetComponent<HeroStats>().towerProtect == false)
                {
                    other.GetComponent<HeroStats>().armor += tower.GetComponent<Tower>().towerProtect;
                    other.GetComponent<HeroStats>().towerProtect = true;

                }
            }
            else
            {

                if (other.GetComponent<CreepMovement>().towerProtection == false)
                {
                    other.GetComponent<Creeps>().armor += tower.GetComponent<Tower>().towerProtect;
                    other.GetComponent<CreepMovement>().towerProtection = true;
                }

            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == faction)
        {
            if (other.name == "Hero")
            {
                if (other.GetComponent<HeroStats>().towerProtect == true)
                {
                    other.GetComponent<HeroStats>().armor -= tower.GetComponent<Tower>().towerProtect;
                    other.GetComponent<HeroStats>().towerProtect = false;
                }
            }
            else
            {

                if (other.GetComponent<CreepMovement>().towerProtection == true)
                {
                    other.GetComponent<Creeps>().armor -= tower.GetComponent<Tower>().towerProtect;
                    other.GetComponent<CreepMovement>().towerProtection = false;
                }
            }
        }
    }
}
