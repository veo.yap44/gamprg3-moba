﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class HeroStats : MonoBehaviour
{
    [Header("UI Stuff")]
    public TextMeshProUGUI[] strText;
    public TextMeshProUGUI strBonus;
    public TextMeshProUGUI strGrowthText;
    public TextMeshProUGUI[] agilityText;
    public TextMeshProUGUI agilityGrowthText;
    public TextMeshProUGUI agiBonusText;
    public TextMeshProUGUI[] intelText;
    public TextMeshProUGUI itelGrowthText;
    public TextMeshProUGUI itelBonusText;
    public TextMeshProUGUI healthText;
    public TextMeshProUGUI[] healthRegenText;
    public TextMeshProUGUI magicResistText;
    public TextMeshProUGUI manaText;
    public TextMeshProUGUI manaRegenText;
    public TextMeshProUGUI[] armorText;
    public TextMeshProUGUI[] damageText;
    public TextMeshProUGUI[] moveSpeedText;
    public TextMeshProUGUI attackRangeText;
    public TextMeshProUGUI attackSpeedText;
    public TextMeshProUGUI projectileSpeedText;
    public TextMeshProUGUI attackTimeText;
    public TextMeshProUGUI levelText;
    public TextMeshProUGUI healthBarText;
    public TextMeshProUGUI goldText;
    public Image healthBar;
    [Header("Stats")]
    public float str;
    public float strGrowth;
    public float agility;
    public float agilityGrowth;
    public float intel;
    public float itelGrowth;
    public float health;
    public float healthRegen;
    public float magicResist;
    public float mana;
    public float manaRegen;
    public float armor;
    public float damage;
    public float moveSpeed;
    public float attackRange;
    public float attackSpeed;
    public float projectileSpeed;
    public float attackTime;
    public float maxHealth;
    public bool towerProtect;
    public int level;
    public float[] expToLevel;
    public float[] expBounty;
    public float[] respawnTime;
    public float totalExp;
    public int gold=600;
    
    public enum PrimaryAttribute {agi,strg,inteli}
    public PrimaryAttribute attribute;
   
    
    public void Update()
    {
        //Set UI for Hero
        goldText.text = gold.ToString();
        healthBarText.text = this.GetComponent<HealthComponent>().health.ToString("F0") + "/" + maxHealth.ToString("F0");
        levelText.text = level.ToString();
        foreach(TextMeshProUGUI hr in healthRegenText)
        {
            hr.text = healthRegen.ToString() + "/s";
        }
        attackSpeedText.text = attackSpeed.ToString();
        manaRegenText.text = manaRegen.ToString();
        attackRangeText.text = attackRange.ToString();
        
        foreach(TextMeshProUGUI dmg in damageText)
        {
            dmg.text = damage.ToString();
        }
        
        foreach (TextMeshProUGUI a in armorText)
        {
            a.text = armor.ToString();
        }
        foreach (TextMeshProUGUI ms in moveSpeedText)
        {
           ms.text = moveSpeed.ToString();
        }
        foreach (TextMeshProUGUI s in strText)
        {
            s.text = str.ToString();
        }
        strGrowthText.text = "(Gains "+strGrowth+" per level)";
        
        foreach (TextMeshProUGUI a in agilityText)
        {
            a.text = agility.ToString();
        }
        agilityGrowthText.text = "(Gains " + agilityGrowth + " per level)";
        foreach (TextMeshProUGUI i in intelText)
        {
            i.text = intel.ToString();
        }
        itelGrowthText.text = "(Gains " + itelGrowth + " per level)";
        healthBar.fillAmount = this.GetComponent<HealthComponent>().health / maxHealth;
        if(this.GetComponent<HealthComponent>().health<maxHealth)
        {
            this.GetComponent<HealthComponent>().health += healthRegen;
            

        }
    }
    private void Start()
    {
        getStrBonus();
        getAgiBonus();
        getIntelBonus();
        getBaseDmg();
        maxHealth = health;
        this.GetComponent<HealthComponent>().health = health;
        magicResistText.text = magicResist.ToString();
        
    }
    public void getStrBonus()
    {
        //Get Strength Bonus
        health += (str * 20);
        healthRegen += (str * 0.1f);
        magicResist += (str * 0.8f);
        strBonus.text = (str * 20) + " HP, "+ (str * 0.1f) + " HP Regen and "+(str * 0.8f)+" % Magic Resist";
    }
    public void getAgiBonus()
    {
        //Get Agi Bonus
        armor += (agility * 0.16f);
        attackSpeed += (agility * 1);
        moveSpeed += moveSpeed / (agility * 0.5f);
        agiBonusText.text = (agility * 0.16f) + " Armor, " + (agility * 1) + " Attack Speed and " + moveSpeed/ (agility * 0.5f) + " % Movement Speed";
    }
    public void getIntelBonus()
    {
        //Get Intel Bonus
        mana += (intel * 12);
        manaRegen += (intel * 0.05f);
        itelBonusText.text = (intel * 12) + " Mana, " + (intel * 0.05f) + " Mana Regen";
    }
    public float getBaseDmg()
    {
        //Get Base Damage based on attribute
        if(attribute==PrimaryAttribute.agi)
        {
            damage += (agility * 1);
        }
        else if(attribute == PrimaryAttribute.strg)
        {
            damage += (str * 1);
        }
        else if(attribute == PrimaryAttribute.inteli)
        {
            damage += (intel * 1);
        }
        return damage;
    }
    public void gainExp(float exp)
    {
        //Gain Exp
        totalExp += exp;
        //set Exp needed to level up
        float expNeeded = expToLevel[level - 1];
        //Set Exp needed if player if player is level 25
        if (level >=25)
        {
            level = 25;
            expNeeded = expToLevel[24];
        }
        

        //Level up with Exp
        if (totalExp >= expNeeded)
        {
            LevelUp();
            expNeeded = expToLevel[level - 1];
        }

        
    }
    public void LevelUp()
    {
        //Player Level Up 
        level++;
        agility += agilityGrowth;
        intel += itelGrowth;
        str += strGrowth;
        getBaseDmg();
        getStrBonus();
        getIntelBonus();
        getAgiBonus();
        this.GetComponent<HealthComponent>().health = health;
        maxHealth=health;
    }

    public void gainGold(int goldGained)
    {
        //Gain Gold
        gold += goldGained;
    }
}
