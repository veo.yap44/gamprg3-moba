﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerManager : MonoBehaviour
{
    public GameObject[] midTowers;
    public GameObject[] topTowers;
    public GameObject[] botTowers;
    public GameObject ancient;
    public float midTowerTier;
    public float topTowerTier;
    public float botTowerTier;
    // Start is called before the first frame update
    void Start()
    {
        midTowerTier = 0;
        midTowers[0].GetComponent<Tower>().isInvunerable = false;
        midTowers[1].GetComponent<Tower>().isInvunerable = true;
        midTowers[2].GetComponent<Tower>().isInvunerable = true;
        midTowers[3].GetComponent<Barracks>().isInvunerable = true;
        midTowers[4].GetComponent<Barracks>().isInvunerable = true;
        midTowers[5].GetComponent<Tower>().isInvunerable = true;
        midTowers[6].GetComponent<Tower>().isInvunerable = true;
        ancient.GetComponent<Ancient>().isInvunerable = true;

        topTowerTier = 0;
        topTowers[0].GetComponent<Tower>().isInvunerable = false;
        topTowers[1].GetComponent<Tower>().isInvunerable = true;
        topTowers[2].GetComponent<Tower>().isInvunerable = true;
        topTowers[3].GetComponent<Barracks>().isInvunerable = true;
        topTowers[4].GetComponent<Barracks>().isInvunerable = true;
        topTowers[5].GetComponent<Tower>().isInvunerable = true;
        topTowers[6].GetComponent<Tower>().isInvunerable = true;
        ancient.GetComponent<Ancient>().isInvunerable = true;

        botTowerTier = 0;
        botTowers[0].GetComponent<Tower>().isInvunerable = false;
        botTowers[1].GetComponent<Tower>().isInvunerable = true;
        botTowers[2].GetComponent<Tower>().isInvunerable = true;
        botTowers[3].GetComponent<Barracks>().isInvunerable = true;
        botTowers[4].GetComponent<Barracks>().isInvunerable = true;
        botTowers[5].GetComponent<Tower>().isInvunerable = true;
        botTowers[6].GetComponent<Tower>().isInvunerable = true;
        ancient.GetComponent<Ancient>().isInvunerable = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(midTowers[0].GetComponent<HealthComponent>().isDestroyed == true)
        {
            midTowers[1].GetComponent<Tower>().isInvunerable = false;
            ancient.GetComponent<Ancient>().isInvunerable = true;
        }
        if(midTowers[1].GetComponent<HealthComponent>().isDestroyed==true)
        {
            midTowers[2].GetComponent<Tower>().isInvunerable = false;
            ancient.GetComponent<Ancient>().isInvunerable = true;

        }
        if(midTowers[2].GetComponent<HealthComponent>().isDestroyed == true)
        {
            midTowers[3].GetComponent<Barracks>().isInvunerable = false;
            midTowers[4].GetComponent<Barracks>().isInvunerable = false;
            midTowers[5].GetComponent<Tower>().isInvunerable = false;
            midTowers[6].GetComponent<Tower>().isInvunerable = false;
            ancient.GetComponent<Ancient>().isInvunerable = true;
        }
        if  (midTowers[5].GetComponent<HealthComponent>().isDestroyed == true&& midTowers[6].GetComponent<HealthComponent>().isDestroyed == true)
        {
            ancient.GetComponent<Ancient>().isInvunerable = false;
            
        }

        if (topTowers[0].GetComponent<HealthComponent>().isDestroyed == true)
        {
            topTowers[1].GetComponent<Tower>().isInvunerable = false;
        }
        if (topTowers[1].GetComponent<HealthComponent>().isDestroyed == true)
        {
            topTowers[2].GetComponent<Tower>().isInvunerable = false;
            ancient.GetComponent<Ancient>().isInvunerable = true;

        }
        if (topTowers[2].GetComponent<HealthComponent>().isDestroyed == true)
        {
            topTowers[3].GetComponent<Barracks>().isInvunerable = false;
            topTowers[4].GetComponent<Barracks>().isInvunerable = false;
            topTowers[5].GetComponent<Tower>().isInvunerable = false;
            topTowers[6].GetComponent<Tower>().isInvunerable = false;
            ancient.GetComponent<Ancient>().isInvunerable = true;
        }
        if (topTowers[5].GetComponent<HealthComponent>().isDestroyed == true&& topTowers[6].GetComponent<HealthComponent>().isDestroyed == true)
        {
            ancient.GetComponent<Ancient>().isInvunerable = false;
           
        }

        if (botTowers[0].GetComponent<HealthComponent>().isDestroyed == true)
        {
            botTowers[1].GetComponent<Tower>().isInvunerable = false;
            ancient.GetComponent<Ancient>().isInvunerable = true;
        }
        if (botTowers[1].GetComponent<HealthComponent>().isDestroyed == true)
        {
            botTowers[2].GetComponent<Tower>().isInvunerable = false;
            ancient.GetComponent<Ancient>().isInvunerable = true;

        }
        if (botTowers[2].GetComponent<HealthComponent>().isDestroyed == true)
        {
            botTowers[3].GetComponent<Barracks>().isInvunerable = false;
            botTowers[4].GetComponent<Barracks>().isInvunerable = false;
            botTowers[5].GetComponent<Tower>().isInvunerable = false;
            botTowers[6].GetComponent<Tower>().isInvunerable = false;
            ancient.GetComponent<Ancient>().isInvunerable = true;
        }
        if (botTowers[5].GetComponent<HealthComponent>().isDestroyed == true&&botTowers[6].GetComponent<HealthComponent>().isDestroyed == true)
        {
            ancient.GetComponent<Ancient>().isInvunerable = false;
        }

        
    }
}
