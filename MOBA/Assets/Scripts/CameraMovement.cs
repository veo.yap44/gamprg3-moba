﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public float camSpeed = 20f;
    public float borderThickness = 10f;
    public float cameraXLimit;
    public float cameraZLimit;

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;

        if(Input.GetKey("w")|| Input.mousePosition.y <=Screen.height - borderThickness)
        {
            pos.z -= camSpeed * Time.deltaTime;
            
        }
        if (Input.GetKey("s")||Input.mousePosition.y >= borderThickness )
        {
            pos.z += camSpeed * Time.deltaTime;
           
        }
        if (Input.GetKey("d")||Input.mousePosition.x <=  borderThickness )
        {
            pos.x -= camSpeed * Time.deltaTime;
           
        }
        if (Input.GetKey("a")||Input.mousePosition.x >= Screen.height - borderThickness )
        {
            pos.x += camSpeed * Time.deltaTime;
            
        }
        pos.x = Mathf.Clamp(pos.x, -19, cameraXLimit);
        pos.z = Mathf.Clamp(pos.z, -30, cameraZLimit);
        transform.position = pos;

    }
}
