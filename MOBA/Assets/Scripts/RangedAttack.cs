﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedAttack : MonoBehaviour
{
    
    public GameObject bulletInstance;
    public Transform bulletSpawn;



    public void Shoot(GameObject enemy, float dmg,float bulletSpeed)
    {
   
            GameObject firedBullet = Instantiate(bulletInstance, bulletSpawn.position, bulletSpawn.rotation);
            ProjectileBehaviour bullet = firedBullet.GetComponent<ProjectileBehaviour>();
            bullet.Seek(enemy);
        bullet.GetDamage(dmg);
        bullet.GetSpeed(bulletSpeed);
        //bullet.GetDamage(dmg);


    }
}
