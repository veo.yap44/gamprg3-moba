﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

[ExecuteAlways]
public class LightManager : MonoBehaviour
{
    
    [SerializeField] private Light DirectionalLight;
    [SerializeField] private LightPreset Preset;
    
    [SerializeField, Range(0, 600)] private float TimeOfDay;
    private TimeSpan timePlaying;
    public Image timeOfDay;
    public Sprite sun;
    public Sprite moon;

    private void FixedUpdate()
    {
       
        if (Preset == null)
            return;

        if (Application.isPlaying)
        {
            
            
            StartCoroutine("counter");
            TimeOfDay %= 600;
            UpdateLighting(TimeOfDay / 600);
        }
        else
        {
            UpdateLighting(TimeOfDay / 600);
        }
        if (TimeOfDay > 300)
        {
            timeOfDay.sprite = sun;
        }
        else if (TimeOfDay < 300)
        {
            timeOfDay.sprite = moon;
        }
    }


    private void UpdateLighting(float timePercent)
    {
        //Updates lighting based on time
        RenderSettings.ambientLight = Preset.AmbientColor.Evaluate(timePercent);
        RenderSettings.fogColor = Preset.FogColor.Evaluate(timePercent);

       
        if (DirectionalLight != null)
        {
            DirectionalLight.color = Preset.DirectionalColor.Evaluate(timePercent);

            DirectionalLight.transform.localRotation = Quaternion.Euler(new Vector3((timePercent * 360f) - 90f, 170f, 0));
        }

    }

    
    private void Checker()
    {
        // Checks if there is a light
        if (DirectionalLight != null)
            return;

        
        if (RenderSettings.sun != null)
        {
            DirectionalLight = RenderSettings.sun;
        }
        
        else
        {
            Light[] lights = GameObject.FindObjectsOfType<Light>();
            foreach (Light light in lights)
            {
                if (light.type == LightType.Directional)
                {
                    DirectionalLight = light;
                    return;
                }
            }
        }
    }

    IEnumerator counter()
    {
        TimeOfDay += Time.deltaTime;
        yield return new WaitForSeconds(1.0f);
    }
}
