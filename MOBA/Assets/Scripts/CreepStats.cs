﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "Creep Stats", menuName = "Scriptables/Creep Stats", order = 2)]
public class CreepStats : ScriptableObject
{

    public float health;
    public float armor;
    public float goldBounty;
    public float moveSpeed;
    public float attackDamage;
    public float exp;
    public float attackRange;
    public float magicResistance;
    public float attackTime;

}
