﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "Structure Stats", menuName = "Scriptables/Structure Stats", order = 3)]
public class StructureStats : ScriptableObject
{
    public float health;
    public float armor;
    public float damage;
    public float attackTime;
    public float attacckRange;
    public float projectileSpeed;
    public float teamGoldBounty;
    public float lastHitBounty;
    public float armorBonus;
    public float hpRegen;
    public float magicResist;
}
