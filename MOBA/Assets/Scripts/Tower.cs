﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public enum towerType { Mid, Top,Bot };
    public towerType Type;
    public string enemyFaction;
    public bool isInvunerable;

    public GameObject target;

    public Creeps targetEnemy;
    public float range;

    public GameObject bulletInstance;
    public float fireRate = 1.0f;
    private float shootNext = 0f;

    public GameObject[] enemies;
   

    public Transform bulletSpawn;

    public StructureStats stats;
    public float health;
    public float armor;
    public float damage;
    public float attackTime;
    public float attacckRange;
    public float projectileSpeed;
    public float teamGoldBounty;
    public float lastHitBounty;
    public float armorBonus;
    public float hpRegen;
    public float magicResist;
    public float towerProtect;

    void TargetUpdate()
    {
        //Updates Target list and target
        enemies = GameObject.FindGameObjectsWithTag(enemyFaction);

        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if (nearestEnemy != null && shortestDistance <= attacckRange)
        {
            target = nearestEnemy;
            
        }
        else
        {
            target = null;
        }

    }

    void Start()
    {
        health = stats.health;
        armor = stats.armor;
        damage = stats.damage;
        attackTime = stats.attackTime;
        attacckRange = stats.attacckRange;
        projectileSpeed = stats.projectileSpeed;
        teamGoldBounty = stats.teamGoldBounty;
        lastHitBounty = stats.lastHitBounty;
        armorBonus = stats.armorBonus;
        hpRegen = stats.hpRegen;
        magicResist = stats.magicResist;

        this.gameObject.GetComponent<HealthComponent>().health = health;
        InvokeRepeating("TargetUpdate", 0f, 0.6f);

    }

    // Update is called once per frame
    void Update()
    {
        health = this.gameObject.GetComponent<HealthComponent>().health;

        if(target!=null)
        {
            if (shootNext <= 0f)
            {
                Shoot();
                shootNext = 1f / fireRate;
            }

            shootNext -= Time.deltaTime;
        }
        if(target ==null)
        {
            TargetUpdate();
        }
      
    }
 
    void Shoot()
    {
        //Shoot Projectile
        GameObject firedBullet = (GameObject)Instantiate(bulletInstance, bulletSpawn.position, bulletSpawn.rotation);
        ProjectileBehaviour bullet = firedBullet.GetComponent<ProjectileBehaviour>();

        if (bullet != null)
        {
            bullet.Seek(target);
            bullet.GetDamage(damage);
            bullet.speed = projectileSpeed;
        }
        
            
    }
    


}
