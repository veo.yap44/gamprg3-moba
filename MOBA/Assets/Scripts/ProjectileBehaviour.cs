﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBehaviour : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject target;
    public float timer;
    public float speed = 10f;

    public float damage;

    public void GetSpeed(float spd)
    {
        speed = spd;
    }
    public void GetDamage(float dmg)
    {
        damage = dmg;
    }

    public void Seek(GameObject _target)
    {
        target = _target;
    }

    // Update is called once per frame
    void Update()
    {
        

        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.transform.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {
            Damage(target);

            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        transform.LookAt(target.transform.position);

    }


    void Damage(GameObject enemy)
    {
        

        if (enemy != null)
        {
          
           enemy.GetComponent<HealthComponent>().TakeDamage(damage);
      
        }
        Destroy(gameObject);


    }

}
