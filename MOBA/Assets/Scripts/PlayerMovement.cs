﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using TMPro;

public class PlayerMovement : MonoBehaviour
{
    public GameObject mesh;
    public GameObject arrow;
    public NavMeshAgent hero;
    public bool stop;
    public float rotSpeed = 0.0075f;
    public float rotVelocity;
    private HeroAttack heroAttack;
    private Animation anim;
    public GameObject[] spawnPoint;
    public bool isDead;
    public float timer;
    public Image respawnTimer;
    public TextMeshProUGUI respawnText;
    private bool check;
    // Start is called before the first frame update
    void Start()
    {
        check = false;
        stop = false;
        anim = this.GetComponent<Animation>();
        hero = gameObject.GetComponent<NavMeshAgent>();
        heroAttack = GetComponent<HeroAttack>();
        anim.Play("Happy Idle");
        respawnTimer.enabled = false;
        respawnText.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (heroAttack.targetedEnemy != null)
        {
            if (heroAttack.targetedEnemy.GetComponent<HeroAttack>() != null)
            {
                if (!heroAttack.targetedEnemy.GetComponent<HeroAttack>().isHeroAlive)
                {
                    heroAttack.targetedEnemy = null;
                }
            }

        }
        if(this.GetComponent<HeroStats>().moveSpeed>=5.5)
        {
            this.GetComponent<HeroStats>().moveSpeed = 5.5f;
        }
        if (Input.GetMouseButtonDown(0))
        {
            if (isDead == false)
            {
                stop = false;
                hero.speed = this.GetComponent<HeroStats>().moveSpeed;
                RaycastHit hit;
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity))
                {

                    
                    var dir = Instantiate(arrow, hit.point, Quaternion.identity);
                    anim.Play("Standard Walk");
                    if (hit.collider.tag == "Floor")
                    {
                        dir = Instantiate(arrow, hit.point, Quaternion.identity);
                        hero.SetDestination(hit.point);
                        heroAttack.targetedEnemy = null;
                        hero.stoppingDistance = 0;
                        
                        Quaternion lookAt = Quaternion.LookRotation(hit.point - transform.position);
                        float rotationY = Mathf.SmoothDampAngle(transform.eulerAngles.y, lookAt.eulerAngles.y, ref rotVelocity, rotSpeed * (Time.deltaTime * 1));
                        transform.eulerAngles = new Vector3(0, rotationY, 0);

                    }
                    Destroy(dir, 0.5f);

                }
            }
           
           
           
        }
        if (isDead == true)
        {
            if(check == false)
            {
                timer = this.gameObject.GetComponent<HeroStats>().respawnTime[this.gameObject.GetComponent<HeroStats>().level - 1];
                check = true;
            }
           
            timer -= Time.deltaTime;
            
            respawnText.text = timer.ToString("F0");
            if (timer <= 0)
            {
                
                this.mesh.GetComponent<SkinnedMeshRenderer>().enabled = true;
                this.gameObject.GetComponent<SphereCollider>().enabled = true;
                this.gameObject.GetComponent<CapsuleCollider>().enabled = true;
                this.gameObject.GetComponent<HeroStats>().health = this.gameObject.GetComponent<HeroStats>().maxHealth;
                hero.speed = this.GetComponent<HeroStats>().moveSpeed;
                timer = 0;
                isDead = false;
                check = false;
                respawnText.enabled = false;
               respawnTimer.enabled = false;
            }
            if (this.gameObject.GetComponent<HeroStats>().level == 25)
            {
                if (timer <=0 )
                {
                    this.mesh.GetComponent<SkinnedMeshRenderer>().enabled = true;
                    this.gameObject.GetComponent<SphereCollider>().enabled = true;
                    this.gameObject.GetComponent<CapsuleCollider>().enabled = true;
                    this.gameObject.GetComponent<HeroStats>().health = this.gameObject.GetComponent<HeroStats>().maxHealth;
                    hero.speed = this.GetComponent<HeroStats>().moveSpeed;
                    timer = 0;
                    isDead = false;
                    check = false;
                    respawnText.enabled = false;
                    respawnTimer.enabled = false;
                }
            }

        }

        if (hero.remainingDistance == hero.stoppingDistance)
        {
            anim.Play("Happy Idle");
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            anim.Play("Happy Idle");
            stop = true;
        }
        if(stop == true)
        {
            hero.speed = 0;
        }
    }
}
