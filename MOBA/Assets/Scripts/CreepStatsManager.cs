﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreepStatsManager : MonoBehaviour
{
    public float timer;
    public int time;
    public CreepStats meleeStats;
    public CreepStats rangedStats;
    public CreepStats siegeStats;

    public CreepStats superMeleeStats;
    public CreepStats superRangedStats;
    public CreepStats superSiegeStats;


    // Start is called before the first frame update
    void Start()
    {
        meleeStats.health = 550f;
        meleeStats.armor = 2f;
        meleeStats.attackDamage =21f;
        meleeStats.attackTime = 1f;
        meleeStats.attackRange = 10f;
        meleeStats.goldBounty = 35f;
        meleeStats.exp = 57f;
        meleeStats.moveSpeed = 3.25f;
        meleeStats.magicResistance = 0;

        rangedStats.health = 300f;
        rangedStats.armor = 0f;
        rangedStats.attackDamage = 24f;
        rangedStats.attackTime = 1f;
        rangedStats.attackRange = 50f;
        rangedStats.goldBounty = 52f;
        rangedStats.exp = 69f;
        rangedStats.moveSpeed = 10f;
        rangedStats.magicResistance = 0;

        siegeStats.health = 875f;
        siegeStats.armor = 0f;
        siegeStats.attackDamage = 41f;
        siegeStats.attackTime = 3f;
        siegeStats.attackRange = 69f;
        siegeStats.goldBounty = 73f;
        siegeStats.exp = 88f;
        siegeStats.moveSpeed = 3.25f;
        siegeStats.magicResistance = 0.8f;

        superMeleeStats.health = 700f;
        superMeleeStats.armor = 3f;
        superMeleeStats.attackDamage = 40f;
        superMeleeStats.attackTime = 1f;
        superMeleeStats.attackRange = 10f;
        superMeleeStats.goldBounty = 22f;
        superMeleeStats.exp = 25f;
        superMeleeStats.moveSpeed = 3.25f;
        superMeleeStats.magicResistance = 0;

        superRangedStats.health = 475f;
        superRangedStats.armor = 1f;
        superRangedStats.attackDamage = 44f;
        superRangedStats.attackTime = 1f;
        superRangedStats.attackRange = 50f;
        superRangedStats.goldBounty = 52f;
        superRangedStats.exp = 22f;
        superRangedStats.moveSpeed = 3.25f;
        superRangedStats.magicResistance = 0;

        superSiegeStats.health = 875f;
        superSiegeStats.armor = 0f;
        superSiegeStats.attackDamage = 41f;
        superSiegeStats.attackTime = 3f;
        superSiegeStats.attackRange = 69f;
        superSiegeStats.goldBounty += 73f;
        superSiegeStats.exp = 88f;
        superSiegeStats.moveSpeed = 3.25f;
        superSiegeStats.magicResistance = 0.8f;
    }

    // Update is called once per frame
    void Update()
    {
        //Updates Creep Stats
        StartCoroutine("Counter");
        if (timer > 450f)
        {
            meleeStats.health += 12;
            meleeStats.attackDamage += 1;
            meleeStats.goldBounty += 1;
            meleeStats.exp += 0;

            rangedStats.health += 12;
            rangedStats.attackDamage += 2;
            rangedStats.goldBounty += 6;
            rangedStats.exp += 8;
            if(this.gameObject.GetComponent<Spawner>().superCreepsEnabled == true)
            {
                superMeleeStats.health += 19;
                superMeleeStats.attackDamage += 2;
                superMeleeStats.goldBounty += 2;
                superMeleeStats.exp += 0;

                superRangedStats.health += 18;
                superRangedStats.attackDamage += 3;
                superRangedStats.goldBounty += 6;
                superRangedStats.exp += 8;
            }
            else
            {
                superMeleeStats.health += 0;
                superMeleeStats.attackDamage += 0;
                superMeleeStats.goldBounty += 0;
                superMeleeStats.exp += 0;

                superRangedStats.health += 0;
                superRangedStats.attackDamage += 0;
                superRangedStats.goldBounty += 0;
                superRangedStats.exp += 0;
            }
            
            timer = 0f;
        }
        
        


    }
    IEnumerator Counter()
    {
        timer+= Time.deltaTime;
        yield return new WaitForSeconds(1.0f);
    }
}
