﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creeps : MonoBehaviour
{ 

    public float health;
    public float armor;
    public float goldBounty;
    public float moveSpeed;
    public float attackDamage;
    public float exp;
    public float attackRange;
    public float magicResistance;
    public float attackTime;
    public float defaultSpeed;
    
    public CreepStats stats;


    void Start()
    {
        defaultSpeed = stats.moveSpeed;
        health = stats.health;
        armor = stats.armor;
        goldBounty = stats.goldBounty;
        moveSpeed = stats.moveSpeed;
        attackDamage = stats.attackDamage;
        exp = stats.exp;
        attackRange = stats.attackRange;
        magicResistance = stats.magicResistance;
        attackTime = stats.attackTime;

        this.gameObject.GetComponent<HealthComponent>().health = health;
    }

// Update is called once per frame
void Update()
    {
        health = this.gameObject.GetComponent<HealthComponent>().health;

    }
    public void TakeDamage(float dmg)
    {
        health -= dmg;
        if(health<=0)
        {
            Die();
        }
    }
    public void Die()
    {
       
        Destroy(this.gameObject);
    }
    
    

}
