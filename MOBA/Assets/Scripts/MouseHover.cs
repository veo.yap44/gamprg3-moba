﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class MouseHover : MonoBehaviour,IPointerEnterHandler,IPointerExitHandler
{
    public Canvas details;
    
    // Start is called before the first frame update
    void Start()
    {
        details.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
  
    public void OnPointerEnter(PointerEventData pointerEventData)
    {
       
        details.enabled = true;
    }
    //Detect when Cursor leaves the GameObject
    public void OnPointerExit(PointerEventData pointerEventData)
    {
        Debug.Log("sada", gameObject);
        details.enabled = false;
    }
}
