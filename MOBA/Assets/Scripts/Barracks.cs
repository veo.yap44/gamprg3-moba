﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barracks : MonoBehaviour
{
    public bool isInvunerable;

    public StructureStats stats;
    public float health;
    public float armor;
    public float damage;
    public float attackTime;
    public float attacckRange;
    public float projectileSpeed;
    public float teamGoldBounty;
    public float lastHitBounty;
    public float armorBonus;
    public float hpRegen;
    public float magicResist;


    void Start()
    {

        health = stats.health;
        armor = stats.armor;
        damage = stats.damage;
        attackTime = stats.attackTime;
        attacckRange = stats.attacckRange;
        projectileSpeed = stats.projectileSpeed;
        teamGoldBounty = stats.teamGoldBounty;
        lastHitBounty = stats.lastHitBounty;
        armorBonus = stats.armorBonus;
        hpRegen = stats.hpRegen;
        magicResist = stats.magicResist;

        this.gameObject.GetComponent<HealthComponent>().health = health;

    }

    // Update is called once per frame
    void Update()
    {
        health = this.gameObject.GetComponent<HealthComponent>().health;

    }
    public void TakeDamage(float dmg)
    {
        health -= dmg;
        if (health <= 0)
        {
            Die();
        }
    }
    public void Die()
    {

        this.gameObject.GetComponent<MeshRenderer>().enabled = false;
        this.gameObject.GetComponent<BoxCollider>().enabled = false;
    }
}
