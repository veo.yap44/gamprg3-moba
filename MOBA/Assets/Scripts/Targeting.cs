﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Targeting : MonoBehaviour
{
    public GameObject selectedHero;
    public bool heroPlayer;
    RaycastHit hit;
    public string enemyFaction;

    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        //Minion Targeting
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity))
            {
                //If the minion is targetable
                if (hit.collider.GetComponent<Targets>() != null)
                {
                    if (hit.collider.gameObject.tag == enemyFaction)
                    {
                       selectedHero.GetComponent<HeroAttack>().targetedEnemy = hit.collider.gameObject;
                    }
                    
                }

                else if (hit.collider.gameObject.GetComponent<Targets>() == null)
                {
                    selectedHero.GetComponent<HeroAttack>().targetedEnemy = null;
                }
            }
        }
        
        if (this.gameObject.GetComponent<HeroAttack>().heroType == HeroAttack.HeroType.Ranged)
        {
            this.GetComponent<SphereCollider>().radius = 4;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == enemyFaction)
        {
            selectedHero.GetComponent<HeroAttack>().targetedEnemy = other.gameObject;
        }
    }
}
