﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CreepMovement : MonoBehaviour
{
    public Transform target;
    private int index = 0;
    public List<GameObject> points;
    public string waypointTags;
    public Animator animator;
    public bool enemySpotted;
    public GameObject enemy;
    public string enemyFaction;
    public float timer;
    private float shootNext = 0f;
    NavMeshAgent creep;
    public bool towerProtection;


    void Start()
    {
        creep = this.GetComponent<NavMeshAgent>();
        points = new List<GameObject>();
        points.AddRange(GameObject.FindGameObjectsWithTag(waypointTags));
        target = points[0].transform;
        this.GetComponent<Animator>();
        this.enemySpotted = false;
        creep.speed = 3.25f;
        creep.stoppingDistance = 0.5f;
    }

    void Update()
    {
        //Move to Waypoint
        if (this.enemySpotted == false)
        {
            animator.Play("Walk");
            Vector3 dir = target.position - transform.position;
            creep.SetDestination(target.position);

            transform.LookAt(target);
            if (!creep.pathPending)
            {
                if (creep.pathStatus == NavMeshPathStatus.PathComplete &&
              !creep.pathPending && creep.remainingDistance <= creep.stoppingDistance)
                {
                    GetNextWaypoint();
                }
            }
        }
        else
        {
            if (this.enemy == null)
            {
                enemySpotted = false;
                creep.stoppingDistance = 0.5f;
                creep.speed = 3.5f;

            }
            //Move to Enemy and Attack
            if (this.enemy != null)
            {
                animator.Play("Attack01");
                Vector3 dir = enemy.transform.position - transform.position;

                creep.SetDestination(enemy.transform.position);
                transform.LookAt(this.enemy.transform);
                creep.stoppingDistance = this.gameObject.GetComponent<Creeps>().attackRange;
                Debug.Log("Here1");
                if (creep.remainingDistance <= this.gameObject.GetComponent<Creeps>().attackRange)
                {
                    creep.speed = 0f;
                    creep.stoppingDistance = this.gameObject.GetComponent<Creeps>().attackRange;
                    Debug.Log("Here2");
                    if (this.gameObject.name == "Ranged(Clone)" || this.gameObject.name == "Siege(Clone)" || this.gameObject.name == "Super Ranged(Clone)" || this.gameObject.name == "Super Siege(Clone)")
                    {
                        Debug.Log("Attacking1");
                        timer += Time.deltaTime;
                        if (timer > this.GetComponent<Creeps>().attackTime)
                        {

                            this.gameObject.GetComponent<RangedAttack>().Shoot(enemy, this.gameObject.GetComponent<Creeps>().attackDamage, 10f);
                            if (this.enemy.GetComponent<HealthComponent>().health <= 0)
                            {
                                creep.speed = 3.25f;
                                this.enemy = null;
                            }
                            timer = 0;

                        }


                    }
                    else
                    {
                        timer += Time.deltaTime;
                        if (timer > this.GetComponent<Creeps>().attackTime)
                        {
                            Debug.Log("Attacking");
                            this.enemy.GetComponent<HealthComponent>().TakeDamage(this.gameObject.GetComponent<Creeps>().attackDamage);
                            if (this.enemy.GetComponent<HealthComponent>().health <= 0)
                            {
                                creep.speed = 3.25f;
                                this.enemy = null;
                            }


                            timer = 0f;
                        }
                    }

                }
            }
        }

    }

    void GetNextWaypoint()
    {
        if (index >= points.Count - 1)
        {

            return;
        }

        index++;
        target = points[index].transform;

    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == enemyFaction)
        {

            this.enemySpotted = true;
            this.enemy = other.gameObject;
            creep.SetDestination(enemy.transform.position);
            transform.LookAt(this.enemy.transform);

        }

    }
    private void OnTriggerStay(Collider other)
    {

        if (other.gameObject.tag == enemyFaction)
        {

            this.enemySpotted = true;
            this.enemy = other.gameObject;
            creep.SetDestination(enemy.transform.position);

        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == enemyFaction)
        {
            creep.speed = 3.5f;

            this.enemySpotted = false;
            this.enemy = null;
        }

    }
}










