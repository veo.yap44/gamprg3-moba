﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitch : MonoBehaviour
{
    public GameObject cam;
    public bool onPlayer;
    // Start is called before the first frame update
    void Start()
    {
        cam.GetComponent<CameraFollow>().enabled = true;
        cam.GetComponent<CameraMovement>().enabled = false;
        onPlayer = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(onPlayer==true)
            {
                cam.GetComponent<CameraFollow>().enabled = false;
                cam.GetComponent<CameraMovement>().enabled = true;
                onPlayer = false;
            }
            else if(onPlayer==false)
            {
                cam.GetComponent<CameraFollow>().enabled = true;
                cam.GetComponent<CameraMovement>().enabled = false;
                onPlayer = true;
            }
        }
    }
}
