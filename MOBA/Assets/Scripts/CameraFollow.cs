﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform hero;
    private Vector3 camOffset;

    [Range(0.1f, 1.0f)]
    public float smooth;
    // Start is called before the first frame update
    void Start()
    {
        camOffset = transform.position - hero.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 newPos = hero.position + camOffset;
        transform.position = Vector3.Slerp(transform.position,newPos,smooth);
    }
}
